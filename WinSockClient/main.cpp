#include <GlobalIncludes.hpp>
#include <iostream>
#include <stdexcept>
#include "Client/Client.hpp"

int main(int argc, char* argv[])
{
	const char* target = "-1.-1.-1.-1";
	// DEBUG:
	target = "127.0.0.1";

	const char* port = cg_Default_Port;
	// Check command line arguments
	if (argc > 1)
	{
		for (int i = 1; i < argc; ++i)
		{
			std::string arg = argv[i];
			if ((arg == "-l") || (arg == "--localhost"))
			{
				target = "127.0.0.1";
			}
			else if ((arg == "--ip") || (arg == "-i"))
			{
				if (i < argc + 1)
				{
					std::string ip = argv[i + 1];
					target = ip.c_str();
				}
			}
			else if ((arg == "--port") || (arg == "-p"))
			{
				if (i < argc + 1)
				{
					std::string portstring = argv[i + 1];
					port = portstring.c_str();
				}
			}
		}
	}
	CONN->Data().SetDestinationIP(target);
	CONN->Data().SetDestinationPort(port);
	CONN->Connect("dsa", "dsa");

	while (true)
	{

	}
	return 0;
}