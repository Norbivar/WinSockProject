#pragma once
#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <GlobalIncludes.hpp>
#include <ByteBuffer.hpp>
#include <OPCodes.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")
#include <thread>
#include <unordered_map>
#include <set>
#include <shared_mutex>
#include <memory>

#define ReadLockGuard(mtx) std::shared_lock<std::shared_mutex>	lock(mtx);
#define WriteLockGuard(mtx) std::unique_lock<std::shared_mutex> lock(mtx);
namespace VN
{
	class Client
	{
		class ClientData
		{
		public:
			ClientData() : m_ValidConnection(false),
				           m_ClientID(-1),
				           m_ClientFlags(0)
			{ }	
			// ClientData exists solely with a Client and thus cannot be copied or moved (as the client).
			ClientData(const ClientData& other) = delete;
			ClientData(ClientData&& other) = delete;
			ClientData& operator=(const ClientData& other) = delete;
			ClientData& operator=(ClientData&& other) = delete;
			
			void SetConnectionValid(bool to)            { WriteLockGuard(DataMutex); m_ValidConnection = to; }
			void SetClientID(int32_t to)                { WriteLockGuard(DataMutex); m_ClientID = to; }
			void SetDestinationIP(const char* target)   { WriteLockGuard(DataMutex); m_DestinationIP = target; }
			void SetDestinationPort(const char* target) { WriteLockGuard(DataMutex); m_DestinationPort = target; }

			bool IsValidConnection() const          { ReadLockGuard(DataMutex); return m_ValidConnection; }
			int32_t GetClientID()	const           { ReadLockGuard(DataMutex); return m_ClientID; }
			const char* GetDestinationIP() const    { ReadLockGuard(DataMutex); return m_DestinationIP.c_str(); }
			const char* GetDestinationPort() const  { ReadLockGuard(DataMutex); return m_DestinationPort.c_str(); }
			
			// An exception to the rule I followed. This is to be more easily accessible and usable.
			// Like if(...GetClientFlags() & cflag_IsLoggedIn) ...
			uint16_t GetClientFlags() const         { ReadLockGuard(DataMutex); return m_ClientFlags; }
			
			// Sets whether the specified sMSG packet ids ignored, depending on the boolean.
			void SetIgnorePacketID(uint16_t packetid, bool IgnoreIfTrue) { if (IgnoreIfTrue) m_BlockedPacketIDs.emplace(packetid); else m_BlockedPacketIDs.erase(packetid); }

			// Makes the server only accept a specific packet id and nothing else, for this client.
			void SetAcceptOnlyThisPacketID(uint16_t packetid)
			{
				SetIgnorePacketID(packetid, false);
				for (uint16_t i = ServerPackets::sBegin; i < ServerPackets::sEnd; ++i)
					if (i != packetid)
						SetIgnorePacketID(i, true);
			}

		private:
			int32_t m_ClientID;
			uint16_t m_ClientFlags;
			bool m_ValidConnection;
			std::string m_DestinationIP;
			std::string m_DestinationPort;

			// This is used with packet ids to enable/disable handing of a certain packet.
			std::set<uint16_t> m_BlockedPacketIDs;

			mutable std::shared_mutex DataMutex;
		};

	public:
		static Client* GetInstance();

		Client();
		~Client();
		// Client is a singleton, therefore I disallow every operation that could mess it up - and I don't even need
		Client(const Client& other) = delete;
		Client(Client&& other) = delete;
		Client& operator=(const Client& other) = delete;
		Client& operator=(Client&& other) = delete;

		// Tries connecting to the IP address stored in Data().GetDestinationIP().
		bool Connect(const char* name, const char* pass);
		bool Disconnect();

		ClientData& Data()             { return m_Data; }
		const ClientData& Data() const { return m_Data; }

	private:
		bool Send(const ByteBuffer& bf);
		void Receive();

		WSADATA wsaData;
		SOCKET ConnectSocket = INVALID_SOCKET;
		struct addrinfo* result = NULL, *ptr = NULL, hints;
		char SendBuffer[cg_Buffer_Length + cg_Buffer_Offset];
		char ReceiveBuffer[cg_Buffer_Length + cg_Buffer_Offset];

		ClientData m_Data;
		std::unique_ptr<std::thread> m_Communication_Thread;
	};
}