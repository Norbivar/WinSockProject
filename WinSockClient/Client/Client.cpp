

#include <string>

#include "Client.hpp"
#include <OPCodes.h>

namespace VN
{
	Client* Client::GetInstance()
	{
		static Client instance;
		return &instance;
	}
	//================================================================================================================
	Client::Client() { }
	Client::~Client()
	{
		Disconnect();
		m_Communication_Thread->join();
	}

	bool Client::Connect(const char* name, const char* pass)
	{
		printf("Starting up...			");
		// Initialize Winsock
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0)
		{
			printf("WSAStartup failed with error: %d\n", iResult);
			return false;
		}
		printf("SUCCESS!\n");
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_UNSPEC;
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;

		printf("Resolving address...		");
		// Resolve the server address and port
		iResult = getaddrinfo(Data().GetDestinationIP(), cg_Default_Port, &hints, &result);
		if (iResult != 0)
		{
			printf("getaddrinfo failed with error: %d\n", iResult);
			WSACleanup();
			freeaddrinfo(result);
			return false;
		}
		printf("SUCCESS!\n");
		printf("Connecting to %s:%s	", Data().GetDestinationIP(), cg_Default_Port);
		// Attempt to connect to an address until one succeeds
		for (ptr = result; ptr != NULL; ptr = ptr->ai_next)
		{
			// Create a SOCKET for connecting to server
			ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
				ptr->ai_protocol);
			if (ConnectSocket == INVALID_SOCKET)
			{
				printf("socket failed with error: %ld\n", WSAGetLastError());
				WSACleanup();
				return false;
			}

			// Connect to server.
			iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
			if (iResult == SOCKET_ERROR)
			{
				closesocket(ConnectSocket);
				ConnectSocket = INVALID_SOCKET;
				printf("ERROR: Destination not responding!\n\n");
				continue;
			}
			printf("SUCCESS!\n");
			break;
		}
		freeaddrinfo(result);
		if (ConnectSocket == INVALID_SOCKET)
		{
			printf("Connection was unsuccessful!\n");
			WSACleanup();
			return false;
		}
		m_Communication_Thread = std::make_unique<std::thread>(&Client::Receive, this);
		printf("Connection is live.\n");
		
		Data().SetAcceptOnlyThisPacketID(sMSG_LoginResult | sMSG_ServerIsFull);
		VN::ByteBuffer loginPacket;
		loginPacket << ClientPackets::cMSG_RequestAuth << name << pass;
		Send(loginPacket);

		return true;
	}
	bool Client::Disconnect()
	{
		// shutdown the connection since program is closing
		if (ConnectSocket != INVALID_SOCKET)
		{
			int iResult = shutdown(ConnectSocket, SD_SEND);
			if (iResult == SOCKET_ERROR)
			{
				printf("shutdown failed with error: %d\n", WSAGetLastError());
				return false;
			}
			closesocket(ConnectSocket);
			WSACleanup();
			ConnectSocket = INVALID_SOCKET;
			printf("Connection closed without error!\n");
			return true;
		}
	}
	bool Client::Send(const ByteBuffer& bf)
	{
		int iResult = send(ConnectSocket, bf.GetStart(), bf.GetLength(), 0);
		if (iResult == SOCKET_ERROR)
		{
			printf("send failed with error: %d\n", WSAGetLastError());
			closesocket(ConnectSocket);
			WSACleanup();
			return false;
		}
		return true;
	}
	void Client::Receive()
	{
		int iResult;
		do
		{
			iResult = recv(ConnectSocket, ReceiveBuffer, cg_Buffer_Length, 0);
			if (iResult > 0)
			{
				ByteBuffer tmp(ReceiveBuffer);
				//GetEventDirector().HandleEvent(tmp);
			}
			else if (iResult == 0)
			{
				printf("ERROR: Server seems full!\n");
				//TODO: Reconnect
			}
			else
			{
				int error = WSAGetLastError();
				if (error == WSAECONNRESET) // SERVER CLOSED CONNECTION
				{
					printf("Server closed connection!\n");
					//TODO: Reconnect or terminate?
				}
				else
					printf("recv failed with error: %d\n", error);
			}
		} 
		while (iResult > 0);
	}
}