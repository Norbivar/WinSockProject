#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#pragma comment(lib, "Ws2_32.lib")

#include <ByteBuffer.hpp>
#include <GlobalIncludes.hpp>
#include <WinSockHeaders.hpp>
#include <OPCodes.h>

#include <shared_mutex>
#include <mutex>
#include <future>
#include <set>

#define ReadLockGuard(mtx) std::shared_lock<std::shared_mutex> lock(mtx);
#define WriteLockGuard(mtx) std::unique_lock<std::shared_mutex> lock(mtx);

// TODO: Make destructors not throwing
namespace VN
{
	class Client
	{
		class ClientInformation
		{
		public:
			ClientInformation(const char* ipaddr, uint32_t id) :
				m_IP             (ipaddr),
				m_kConnectionID  (id),
				m_ClientID       (-1),
				m_Name           ("Unnamed")
			{ 
				SetAcceptOnlyThisPacketID(ClientPackets::cMSG_RequestAuth);
			}

			ClientInformation(ClientInformation&& other) : 
				m_IP               (std::move(other.m_IP)),
				m_kConnectionID    (other.m_kConnectionID),
				m_ClientID         (other.m_ClientID),
				m_Name             (std::move(other.m_Name)),
				m_BlockedPacketIDs (std::move(other.m_BlockedPacketIDs))
			{ }

			// Because of the std::thread, I only allow Clients to be move-constructed from, so ClientInformation will only be moved.
			ClientInformation(const ClientInformation& other) = delete;
			ClientInformation& operator=(const ClientInformation&) = delete;
			ClientInformation& operator=(ClientInformation&&) = delete;

			// Sets whether the specified packet id (cMSG !!!) is ignored for this client or not, depending on the boolean.
			void SetIgnorePacketID(uint16_t packetid, bool IgnoreIfTrue) { if (IgnoreIfTrue) m_BlockedPacketIDs.emplace(packetid); else m_BlockedPacketIDs.erase(packetid); }

			// Makes the server only accept a specific packet id and nothing else, for this client.
			void SetAcceptOnlyThisPacketID(uint16_t packetid)   
			{ 
				SetIgnorePacketID(packetid, false);
				for (uint16_t i = ClientPackets::cBegin; i < ClientPackets::cEnd; ++i)
					if(i != packetid)
						SetIgnorePacketID(i, true); 	
			}

			void            SetName(const char* to) { WriteLockGuard(DataMutex); m_Name = to; }
			void            SetClientID(int32_t id)	{ WriteLockGuard(DataMutex); if (m_ClientID == -1) m_ClientID = id; }
			//TODO: Use atomics?
			const char*     GetName() const         { ReadLockGuard(DataMutex); return m_Name.c_str(); }
			const char*     GetIP()	const           { ReadLockGuard(DataMutex); return m_IP.c_str(); }
			const int32_t   GetClientID() const     { ReadLockGuard(DataMutex); return m_ClientID; }
			const uint32_t  GetConnectionID() const { ReadLockGuard(DataMutex); return m_kConnectionID; }
			const bool      IsLoggedIn() const      { ReadLockGuard(DataMutex); return (m_ClientFlags & cflag_IsLoggedIn); }
			uint16_t        GetClientFlags() const  { ReadLockGuard(DataMutex); return m_ClientFlags; }

			bool            IsPacketIgnored(uint16_t packetid) { if (m_BlockedPacketIDs.find(packetid) == m_BlockedPacketIDs.end()) return false; else return true; }
		private:
			/* On insertion of new datamembers you must also provide:	- constructor instructions (OPTIONAL : based on the added member) 
																		- getter/setter (of above examples)
																		- entry in Client::Save()			
			*/

			const std::string  m_IP; // I don't want to worry about copying char*s so I used  string because I know it copies the const char* it was created from
			const uint32_t     m_kConnectionID;
			// ClientID is the ID for the Client from the Database which we query. Allowed to be set only once.
			int32_t            m_ClientID;
			// Additional infos...
			std::string        m_Name;
			// This is used with packet ids to enable/disable handing of a certain packet.
			std::set<uint16_t> m_BlockedPacketIDs;
			// Used to hold boolean values that describe the client.
			uint16_t           m_ClientFlags;

			// Eliminating raceconditions on get/set. Sadly I am forced to do it this way since Client::GetInformation() returns only a ref
			// and it's probably optimised away.
			// Sadly this alone does not make the iteration through the set danger-proof since a for(...) may still skip the set.end()
			mutable std::shared_mutex DataMutex;
		};
		public:
		// Rule of 5:
			// Constructs a Client with given ID, IP address and SOCKET*.
			Client(uint32_t id, const char* i, SOCKET* clientsocket) : m_ClientSocket(clientsocket), m_Information(i, id) 
			{
				m_HandlerThread = std::make_unique<std::thread>(&Client::Receive, this);
			}

			// Destroys the client and any associated data members. Also signals the server to remove the connection from the unordered_set.
			~Client();

			// Clients should not be copyable : there are only distinct clients, I really on Copy-Elision and Moving.
			Client(const Client&) = delete;
			Client& operator=(const Client&) = delete;

			// Move can be neccessary: when the clients logs in he get's moved to a new container.
			// Whole new object constructed from an rvalue: temporary OR std::move(...)
			Client(Client&&);

			// Move assignment can be discarded
			// Existing, 'living' object reassigned to an rvalue: temporary OR std::move(...)
			Client& operator=(Client&&) = delete;

			// Sends the argument ByteBuffers content to the client.
			void Send(const ByteBuffer& bf);

			// Accesses the Database and Saves the Client's data.
			void Save() const;

			// The hash is determined by the Client ID (unique because it's the primary key in database) but it is mutexed so can't be constant.
			uint32_t GetHash() const { return m_Information.GetConnectionID(); }

			// Allows access to Client Information.
			ClientInformation& Data()               { return m_Information; }
			const ClientInformation& Data() const   { return m_Information; }

			// Effectively you could read while writing as well but this way it may be much expressive
			// it would be even better, if I could make the getters const, but I cannot because the R-W lock on them.
			//const ClientInformation& Read() const 	{ return m_Information; }
			//ClientInformation& Write() 				{ return m_Information; }
		private:
			// Listens on the ClientSocket for incoming traffic. The HandlerThread mainly handles this. Also it's should only be called from the this->m_HandlerThread
			void Receive();

			SOCKET*                     m_ClientSocket;
			char                        m_ReceiveBuffer[cg_Buffer_Length + cg_Buffer_Offset];
			ClientInformation           m_Information;
			mutable std::future<bool>   m_Future_Save;

			// The number of received and damaged/cheated packets.
			uint16_t                     m_FalsePackets;
			std::unique_ptr<std::thread> m_HandlerThread; // needs to be initialized last
	};
}