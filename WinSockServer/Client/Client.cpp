#include "Client.hpp"
#include <memory>
#include <ByteBuffer.hpp>
#include <GlobalIncludes.hpp>
#include "../Server/Server.hpp"
namespace std 
{
	template<>
	struct hash<VN::Client>
	{
		inline size_t operator()(VN::Client const* c) const
		{
			return c->GetHash();
		}
	};
}
namespace VN
{
	Client::~Client()
	{
		if (*m_ClientSocket != INVALID_SOCKET) // I don't think this is possible but hey... :D
		{
			closesocket(*m_ClientSocket);
			delete m_ClientSocket;
		}
		if(m_HandlerThread->joinable()) // TODO: Validate this
			m_HandlerThread->join();	
	}
	Client::Client(Client&& other) : m_Information(std::move(other.m_Information)), // uses ClientInformations MOVE-CONSTRUCTOR and not MOVE-ASSIGNMENT operator
	                                 m_Future_Save(std::move(other.m_Future_Save)),
		                             m_HandlerThread(std::move(other.m_HandlerThread))
	{
		m_ClientSocket = other.m_ClientSocket;
		other.m_ClientSocket = nullptr;
	}

	void Client::Receive()
	{
		int iResult;
		do
		{
			iResult = recv(*m_ClientSocket, m_ReceiveBuffer, cg_Buffer_Length, 0);
			if (iResult > 0) // if I got something out of this packet
			{
				//TODO: Spawn async task for data update, anything else remains syncronous, like requests 
				const ByteBuffer bf(m_ReceiveBuffer);
				sServer->GetDirector().HandleEvent(*this, bf);
			}
			else if (iResult == 0)
				printf("DEBUG: 0 result...\n");
			else
			{
				// ALSO: seems to receive error 10093 as well
				int error = WSAGetLastError();
				if (error == WSAECONNRESET) // CLIENT closed connection
				{
					delete this; // TODO: might need to rethink this whole thing here.
				}
				else
				{
					printf("Receive error: %d\n", error);
				}
				//closesocket(*m_ClientSocket);
			}

		} while (iResult > 0);
	}
	void Client::Send(const ByteBuffer& bf)
	{
		int iSendResult = send(*m_ClientSocket, bf.GetStart(), bf.GetLength(), 0);
		if (iSendResult == SOCKET_ERROR)
		{
			printf("Send to %s failed with error: %d\n", Data().GetIP(), WSAGetLastError());
			//WSACleanup();
			return;
		}
	}
	void Client::Save() const 
	{
		if (Data().IsLoggedIn()) // don't save temporary clients
		{
			// This will allow us to launch it on a different thread (it's a one-way data save) and continue on communicating.
			// The future will also make sure that the thread is done before destruction, so no data corruption could occur.
			m_Future_Save = std::async(std::launch::async, [&]() 
			{
				bool isSuccessful = false;
				//sServer->GetDatabase().Select("", "", "");
				return isSuccessful;
			});
		}
	}
}