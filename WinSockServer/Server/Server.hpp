#pragma once

#include <condition_variable>	

#include "AuthComponent/AuthComponent.hpp"
#include "DatabaseComponent/DatabaseComponent.hpp"
#include "DirectorComponent/DirectorComponent.hpp"
#include "ConnectionsComponent/ConnectionsComponent.hpp"
namespace VN
{
	class Server
	{
		public:
			Server();
			~Server();

			Server(const Server&) = delete;
			Server operator=(const Server&) = delete;

			Server(Server&&) = delete;
			Server operator=(Server&&) = delete;

			// Starts the AuthComponent's listening (client admission) and begins blocking the mainthread (the only thread it should be called from).
			void Start();

			// Stops listening, then the Blocker thread, thus allowing execution to continue on mainthread : essentially hitting return and quitting.
			// Refer to the implementation of Start() as well.
			void Quit() { std::unique_lock<std::mutex> lk(m_BlockerMutex); m_Blocker.notify_all(); }

			// Saves and Quits afterwards.
			void SaveAndQuit() { SaveAll(); Quit(); }

			// Shortcut to initiate save.
			void SaveAll() const { GetConnections().SaveAllClients(); }

			ConnectionsComponent&   GetConnections()    { return m_ConnectionsComponent; }
			DirectorComponent&      GetDirector()       { return m_DirectorComponent; }
			DatabaseComponent&      GetDatabase()       { return m_DatabaseComponent; }
			AuthComponent&          GetAuth()           { return m_AuthComponent; }

			const ConnectionsComponent&   GetConnections() const  { return m_ConnectionsComponent; }
			const DirectorComponent&      GetDirector() const     { return m_DirectorComponent; }
			const DatabaseComponent&      GetDatabase() const     { return m_DatabaseComponent; }
			const AuthComponent&          GetAuth() const         { return m_AuthComponent; }

			// Creates or retrieves the Server (SINGLETON).
			static Server* instance();
			
		private:
			ConnectionsComponent    m_ConnectionsComponent;
			DirectorComponent       m_DirectorComponent;
			DatabaseComponent       m_DatabaseComponent;
			AuthComponent           m_AuthComponent;

			// The condition variable which blocks the main() thread and it's mutex.
			std::condition_variable  m_Blocker;
			std::mutex               m_BlockerMutex;
	};
}