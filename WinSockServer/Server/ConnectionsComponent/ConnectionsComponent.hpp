#pragma once
#include <mutex>
#include <memory>

#include "../../Client/Client.hpp"


namespace VN
{
	class ConnectionsComponent
	{	
		public:
			ConnectionsComponent();
			~ConnectionsComponent();

			ConnectionsComponent(const ConnectionsComponent&)              = delete;
			ConnectionsComponent& operator=(const ConnectionsComponent&)   = delete;

			ConnectionsComponent(ConnectionsComponent&&)                   = delete;
			ConnectionsComponent& operator=(ConnectionsComponent&&)        = delete;

			auto& GetClients()          { return m_CurrentClients; }
			auto& GetPendingClients()   { return m_PendingClients; }

			const auto& GetClients() const          { return m_CurrentClients; }
			const auto& GetPendingClients() const   { return m_PendingClients; }

			// Saves every active (authenticated) client.
			void SaveAllClients() const;

			// Transfers a Client from 'Pending Clients' to 'Current Clients', effectively notifying of logged in status.
			void SetClientActive(const std::unique_ptr<Client>& who);
			
			uint32_t GetNextFreeConnectionID() { return nextConnectionID++; }
		private:
			// Just a unique number given to each connection.
			uint32_t nextConnectionID = 0;
			// Currently LOGGED IN clients:
			std::set<std::unique_ptr<Client>> m_CurrentClients;
			// Joined, but NOT LOGGED IN clients (for potential timeouts):
			std::set<std::unique_ptr<Client>> m_PendingClients;
	};
}