#include "AuthComponent.hpp"
#include <stdexcept>
#include <memory>
#include <GlobalIncludes.hpp>
#include <Logger.h>

#include "../Server.hpp"
#define DEFAULT_PORT "2701"
namespace VN
{
	AuthComponent::AuthComponent()
	{
		Log->info("	[STARTUP] Initializing Auth Server	");
		int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (iResult != 0)
		{
			throw std::runtime_error("Error during WSAStartup!");
		}

		struct addrinfo hints;
		ZeroMemory(&hints, sizeof(hints));
		hints.ai_family = AF_INET;
		// SOCK_SEQPACKET ???
		hints.ai_socktype = SOCK_STREAM;
		hints.ai_protocol = IPPROTO_TCP;
		hints.ai_flags = AI_PASSIVE;

		// Resolve the server address and port
		iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
		if (iResult != 0)
		{
			WSACleanup();
			throw std::runtime_error("Error during getaddrinfo!");
		}

		// Create a SOCKET for connecting to server
		ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
		if (ListenSocket == INVALID_SOCKET)
		{
			freeaddrinfo(result);
			WSACleanup();
			throw std::runtime_error("Error during assigning ListenSocket!");
		}

		// Setup the TCP listening socket
		iResult = bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);
		if (iResult == SOCKET_ERROR)
		{
			freeaddrinfo(result);
			closesocket(ListenSocket);
			WSACleanup();
			throw std::runtime_error("Error during bindig socket!");
		}
		// Fill up m_MyIPAddress with my IP
		//inet_ntop(AF_INET, &result->ai_addr, m_MyIPAddress, INET_ADDRSTRLEN); // TODO: save ip as string

		freeaddrinfo(result);

		iResult = listen(ListenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR)
		{
			closesocket(ListenSocket);
			WSACleanup();
			throw std::runtime_error("Error during starting listening!");
		}
		Log->info("		SUCCESS!");
	}
	AuthComponent::~AuthComponent()
	{
		Log->info("	[QUITTING] Auth server	");
		StopListening();
		Log->info("		is DOWN!");
	}
	bool AuthComponent::StartListening()
	{
		if(ListenSocket != INVALID_SOCKET) // we have a valid socket
		{
			if(m_ListenerThread == nullptr) // and we don't have a listener thread yet
			{
				m_ListenerThread = new std::thread(&AuthComponent::WaitForConnections, this);
				if (m_ListenerThread)
				{
					Log->info("[STARTUP] Accepting connections on {} !", m_MyIPAddress);
					m_ListenerThread->detach();
					return true;
				}
			}
		}		
		return false;
	}
	void AuthComponent::StopListening()
	{
		acceptingConnections = false; // the Listener should still be blocked in the 'accept()'
		if(ListenSocket != INVALID_SOCKET)
			closesocket(ListenSocket); // this will invalidate the socket so we need to reconstruct it (as in constructor)
			// and as long as we use a blocking 'accept' I can only interrupt it this way.
		if(m_ListenerThread)
		{
			if (m_ListenerThread->joinable())
			{
				m_ListenerThread->join();
				delete m_ListenerThread;
				m_ListenerThread = nullptr;
			}
			else Log->error("[AUTH] Listener Thread could not be joined!");
		}
	}
	void AuthComponent::WaitForConnections()
	{
		acceptingConnections = true;
		while (acceptingConnections)
		{
			SOCKADDR_IN client_info = { 0 };
			int addrsize = sizeof(client_info);

			SOCKET* tmpClientSocket = new SOCKET;
			(*tmpClientSocket) = INVALID_SOCKET;

			(*tmpClientSocket) = accept(ListenSocket, (struct sockaddr*) &client_info, &addrsize); // TODO: upgrade this to non-blocking (select / pool)
			if ((*tmpClientSocket) != INVALID_SOCKET) // Valid incoming connection
			{
				Log->info("[CLIENT] Incoming connection: New");
				char ip[INET_ADDRSTRLEN];
				inet_ntop(AF_INET, &client_info.sin_addr, ip, INET_ADDRSTRLEN);
				uint32_t id = sServer->GetConnections().GetNextFreeConnectionID();

				sServer->GetConnections()
					.GetPendingClients()
					.emplace(
						std::make_unique<Client>(id, ip, tmpClientSocket)
					);
			}
		}
	}
}