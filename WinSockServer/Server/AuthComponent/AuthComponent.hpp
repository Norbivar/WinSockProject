#pragma once
#include <thread>
#include <atomic>

#include <WinSockHeaders.hpp>

namespace VN
{
	class AuthComponent
	{
		public:
			bool StartListening();
			void StopListening();
			void WaitForConnections();

			AuthComponent();
			~AuthComponent();

			AuthComponent(const AuthComponent&) = delete;
			AuthComponent& operator=(const AuthComponent&) = delete;

			AuthComponent(AuthComponent&&) = delete;
			AuthComponent& operator=(AuthComponent&&) = delete;

		private:
			SOCKET ListenSocket = INVALID_SOCKET;
				WSADATA wsaData;
				struct addrinfo* result = NULL;
				std::atomic<bool> acceptingConnections = false;

			std::thread*  m_ListenerThread = nullptr;
			char          m_MyIPAddress[INET_ADDRSTRLEN];
	};
}