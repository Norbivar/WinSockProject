
#include <stdio.h>
#include <stdexcept>

#include <Logger.h>
#include "Server.hpp"
#include "../Libs/SQLAPI/include/SQLAPI.h"
namespace VN
{
	Server::Server()
	{
		// Members get constructed before this, so if this runs then the construction is successful.
		Log->info("[STARTUP] Server SUCCESSFULLY started!");
	}
	Server::~Server() 
	{
		m_AuthComponent.StopListening();
		// The Server is a static, a singleton, so destruction should only happen at main program exit (or implicit delete but...), which can only be achieved
		// by closing the Blocker thread, which in turn also destroys the listener. 
		// This is to ensure that threads get deleted correctly (only owning ones).

		printf("\nServer is CLOSING...\n");
	}
	void Server::Start()
	{
		m_AuthComponent.StartListening();
		Log->info("[STARTUP] Server is accepting connections!");

		std::unique_lock<std::mutex> lk(m_BlockerMutex);
		m_Blocker.wait(lk); // Notified by Server::Quit()

		// ====================================================
		// This part is pretty much run when 'Quit()' is called
		Log->info("[QUITTING] Server is CLOSING...");
		m_AuthComponent.StopListening();
	}

	Server* Server::instance()
	{
		try
		{
			static Server s; return &s;
		}
		catch (std::runtime_error& err)
		{
			Log->error("[STARTUP EXCEPTION]: {}", err.what());
		}
		catch (SAException& err)
		{
			Log->error("[DB EXCEPTION]: {}", err.ErrMessage().GetMultiByteChars());
		}
		return nullptr;
	}
}