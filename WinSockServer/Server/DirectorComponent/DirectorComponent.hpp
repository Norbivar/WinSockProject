#pragma once
#include <map>
#include <ByteBuffer.hpp>
namespace VN
{
	class Client;
	class DirectorComponent
	{
		private:
			typedef void (*EventHandler)	(Client& current);
			typedef void (*PacketHandler)	(Client& current, const ByteBuffer& bf);

			std::map<int, EventHandler>		EventMap;
			std::map<int, PacketHandler>	PacketEventMap;

		public:
			DirectorComponent();
			~DirectorComponent();

			DirectorComponent(const DirectorComponent&) = delete;
			DirectorComponent& operator=(const DirectorComponent&) = delete;

			DirectorComponent(DirectorComponent&&) = delete;
			DirectorComponent& operator=(DirectorComponent&&) = delete;

			void HandleEvent(Client& current, const ByteBuffer& bf);
			void HandleEvent(Client& current, int eventid);
	};
}