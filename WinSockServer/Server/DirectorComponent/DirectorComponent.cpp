
#include <stdio.h>

#include <OPCodes.h>
#include <Logger.h>

#include "../WinSockServer/Client/Client.hpp"
#include "DirectorComponent.hpp"
namespace VN
{
	DirectorComponent::DirectorComponent()
	{
		Log->info("	[STARTUP] Starting up DirectorComponent	");
		//============================ Client Events: =======================================
		EventMap.emplace(cEvent_Join,	[] (Client& /*current*/) -> void
		{
			
		});
		EventMap.emplace(cEvent_Leave,	[] (Client& /*current*/) -> void
		{
	
		});
		//============================ cMSG Events:  ========================================
		// Always called by client's thread.
		PacketEventMap.emplace(cMSG_RequestAuth,	[](Client& current, const ByteBuffer& bf) -> void
		{
			if (current.Data().IsLoggedIn())
				return;

			//Username(str) + Password(str)
			const char* username;
			const char* password;
			bf >> username >> password;

			ByteBuffer lol;


			//if()
		});

		Log->info("		SUCCESS!");
	}
	DirectorComponent::~DirectorComponent()
	{
		Log->info("	[QUITTING] Director	");
		Log->info("		is DOWN!");
	}
	void DirectorComponent::HandleEvent(Client& current, const ByteBuffer& bf)
	{
		uint16_t packetid; // might need to be uint32_t
		bf >> packetid;
		if (current.Data().IsPacketIgnored(packetid))
			return;

		auto HandlerPair = PacketEventMap.find(packetid);
		if (HandlerPair != PacketEventMap.end())		// a handler has been found for the cMSG
		{
			HandlerPair->second(current, bf);	// call the handler (the second of the pair in the map)
		}
		else
		{
			Log->warn("	[WARNING] IGNORED PACKET recieved! FROM: {0} (ID: {1}), PacketID: {2}", current.Data().GetIP(), current.Data().GetClientID(), packetid);
		}
	}
	void DirectorComponent::HandleEvent(Client& current, const int eventid)
	{
		auto HandlerPair = EventMap.find(eventid);
		if (HandlerPair != EventMap.end())
		{
			HandlerPair->second(current);
		}
		else
		{
			Log->warn("	[WARNING] UNHANDLED EVENT recieved! FROM: {0} (ID: {1}), PacketID: {2}", current.Data().GetIP(), current.Data().GetClientID(), eventid);
		}
	}
}