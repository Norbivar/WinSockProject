#pragma once
#include <SQLAPI/include/SQLAPI.h>
#include <vector>
#include <unordered_map>

constexpr uint16_t Sql_CommandText_MaxLength = 512;
namespace VN
{
	class DatabaseComponent
	{
		public:
			DatabaseComponent();
			~DatabaseComponent();

			DatabaseComponent(const DatabaseComponent&) = delete;
			DatabaseComponent& operator=(const DatabaseComponent&) = delete;

			DatabaseComponent(DatabaseComponent&&) = delete;
			DatabaseComponent& operator=(DatabaseComponent&&) = delete;

			// Executes an UPDATE statement on the DB Server, returning the number of affected rows.
			uint32_t Update(const char* table, const char* what);

			// Executes a DELETE statement on the DB server, returning the number of deleted rows.
			uint32_t Delete(const char* table, const char* what);

			// Executes an INSERT statement on the DB server, returning the number of inserted rows.
			uint32_t Insert(const char* table, const char* what);

			// Executes a statement and returns a ResultSet transformed into a more easily usable structure. It should be copy-elided as possible.
			// Each object in the vector is one line in the database.
			std::vector<std::unordered_map<int, std::string>> Select(const char* what, const char* from, const char* where);

			// Executes a statement and returns the affected rows number.
			uint32_t SelectScalar(const char* table, const char* from, const char* where);
		
		private:
			SAConnection m_SqlConnection;
			
	};
}