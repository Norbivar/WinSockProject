#include <string>

#include <Logger.h>
#include "DatabaseComponent.hpp"

namespace VN
{
	DatabaseComponent::DatabaseComponent()
	{
		Log->info("	[STARTUP] Connecting to MySQL server	");
		m_SqlConnection.Connect(
			"cppserver.database.windows.net,1433@ProjectServerDatabase",     // TODO: change this/move to enforce more security
			"Norbivar",   // user name
			"Qwertzui1",   // password
			SA_SQLServer_Client);
		Log->info("		SUCCESS!");
	}
	DatabaseComponent::~DatabaseComponent()
	{
		Log->info("	[QUITTING] Database connection ");
		if(m_SqlConnection.isAlive() && m_SqlConnection.isConnected())
			m_SqlConnection.Disconnect();
			
		Log->info("		is DOWN!");
	}
	 
	uint32_t DatabaseComponent::Update(const char* table, const char* what) // TODO
	{
		return uint32_t();
	}
	uint32_t DatabaseComponent::Delete(const char* table, const char* what) // TODO
	{
		return uint32_t();
	}
	uint32_t DatabaseComponent::Insert(const char* table, const char* what) // TODO
	{
		return uint32_t();
	}
	std::vector<std::unordered_map<int, std::string>> DatabaseComponent::Select(const char* table, const char* from, const char* where)
	{
		std::vector < std::unordered_map <int, std::string> > tmpvector;
		try
		{
			char cmdtext[Sql_CommandText_MaxLength];
			sprintf_s(cmdtext, "SELECT %s FROM %s WHERE %s;", table, from, where);

			SACommand cmd(&m_SqlConnection, cmdtext, SA_CmdSQLStmt);
			printf("[�ebug]: Query is %s (where is '%s')\n", cmd.CommandText(), where);
			cmd.Execute();
			int column_count = cmd.FieldCount();

			while (cmd.FetchNext())
			{
				std::unordered_map<int, std::string> row(column_count);
				for (uint8_t col = 1; col < column_count; ++col)
				{
					row.emplace(
						col,
						std::string(
							static_cast<const char*>(cmd[col].asString())
						)
					);
				}
				tmpvector.push_back(std::move(row));
			}
		}
		catch (SAException& ex)
		{
			printf("[DB ERROR] %s\n", ex.ErrMessage());
		}
		return tmpvector;
	}
	uint32_t DatabaseComponent::SelectScalar(const char* what, const char* from, const char* where)
	{
		SACommand cmd(&m_SqlConnection, what, SA_CmdSQLStmt);
		cmd.Execute();
		return cmd.RowsAffected();
	}
}