#include "Server/Server.hpp"
#include <iostream>
#include <stdexcept>
#include <thread>
#include <chrono>
//Needed only for debug console:
#include <vector>
#include <unordered_map>
#include <string>
//-----------------------------
#include <Logger.h>

void ServerConsoleInput()
{
	std::string str;
	do
	{
		std::getline(std::cin, str);
		if (!str.empty())
		{
			size_t SpacePos = str.find(' ');
			if (str == "help")
				std::cout << "CMD: List of commands: quit | close | save" << std::endl;

			else if (str == "quit" || str == "q")
			{
				sServer->Quit();
				break;
			}
			else if (str == "save")
			{
				sServer->SaveAll();
			}
			else if (str == "sq")
			{
				sServer->SaveAndQuit();
			}
			else if (str.substr(0, SpacePos) == "query")
			{
				//const char* query = str.substr(SpacePos, str.length()).c_str();
				auto Result = sServer->GetDatabase().Select("*", "Users", "id IS NOT NULL");
				for (auto Line : Result)
				{
					for (auto Data : Line)
					{
						std::cout << Data.second << " ";
					}
					std::cout << std::endl;
				}
			}
			else if (str.substr(0, SpacePos) == "inject")
			{
				//const char* query = str.substr(SpacePos, str.length()).c_str();
				auto Result = sServer->GetDatabase().Select("*", "Users", "password='fakker'; INSERT INTO users(id, username, password) VALUES(3,'norbika','kaka')");
				for (auto Line : Result)
				{
					for (auto Data : Line)
					{
						std::cout << Data.second << " ";
					}
					std::cout << std::endl;
				}
			}
			else std::cout << "CMD: Unrecognized command '" << str << "'. Try 'help' for list of commands.\n";
		}
	} 
	while (true);
}

int main(int argc, char* argv[])
{
	VN::Server* server = nullptr;
	while(true)
	{
		Log->info("[STARTUP] Server starting up...");
		server = VN::Server::instance();
		if(server != nullptr)
			break;

		Log->info("\nEncountered an error! Retrying in 10 seconds!\n");
		std::this_thread::sleep_for(std::chrono::seconds(10));
	}
	std::thread input(ServerConsoleInput);
	input.detach();
	server->Start(); // this will block the main thread
	// Execution will continue below a bit after server->Quit();
	return 0;
}
