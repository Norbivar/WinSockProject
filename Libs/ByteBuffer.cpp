#include "ByteBuffer.hpp"
namespace VN
{
	size_t ByteBuffer::GetLength() const
	{
		if(!m_IsReadOnly)
			return m_CurrAtByte;

		uint8_t ToSkip = m_CurrDataNumber;

		if(ToSkip == 0)
			return 0;

		for(size_t i = 0; i < cg_Buffer_Length; ++i)
		{
			if(m_ByteArray[i] == '\0')
			{
				if(--ToSkip == 0)
				{
					return (i+1);
				}
			}
		}
		return 0;
	}

}