#pragma once
//Received [FROM] client [TO] server
enum ClientPackets : unsigned short int
{
	cMSG_KeepAlive = 0x00000001,
	cMSG_RequestAuth, // + Username(str) + Password(str)

	cMSG_End,

	//Helpers:
	cBegin = cMSG_KeepAlive,
	cEnd = cMSG_End
};

//Sent [FROM] server [TO] clients.
enum ServerPackets : unsigned short int
{
	sMSG_ServerIsFull = 0x00000001,
	sMSG_LoginResult, // + a bool, IF true then additional: int clientID . ||| reminder: username can be set if we successfully logged in on client.
	sMSG_Message,
	sMSG_End,

	sBegin = sMSG_ServerIsFull,
	sEnd = sMSG_End
};

enum ConnectionEvents
{
	cEvent_Join = -1000,
	cEvent_Leave
};

// Clientside usage:
// stored in uint16_t
enum ClientFlags
{
	cflag_IsLoggedIn = 0b00000000000000001
};