#pragma once
#include <cstdint>
#define sServer VN::Server::instance()
#define CONN VN::Client::GetInstance()



// This MUST BE SYNCED with the Client's includes.
constexpr const uint16_t cg_Buffer_Length = 512;
constexpr const uint8_t cg_Buffer_Offset = 1;
constexpr const char* cg_Default_Port = "2701";
constexpr const char* const ServerLogFile = "logs/server.txt";
constexpr const char* const ClientLogFile = "logs/clients.txt";