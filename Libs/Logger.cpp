#include <fstream>
#include <iostream>

#include "Logger.h"
namespace VN
{
	Logger* Logger::GetLogger()
	{
		try
		{
			static Logger s;
			return &s;
		}
		catch (const spdlog::spdlog_ex& ex)
		{
			std::cout << ex.what() << std::endl;
			// exception will probably be missing txt files. I haven't found any clue about the logger, so I do it manually
			std::ifstream finServ(ServerLogFile);
			if (!finServ)
			{
				std::ofstream outfile(ServerLogFile);
			}
			static Logger s;
			return &s;
		}
	}
}