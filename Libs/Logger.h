#pragma once
#include "spdlog/spdlog.h"
#include "GlobalIncludes.hpp"
#define Log VN::Logger::GetLogger()
namespace VN
{
	class Logger
	{
	public:
		static Logger* GetLogger();
		Logger() :
			RotatedServerLogger          (spdlog::rotating_logger_mt("ServerLogger", ServerLogFile, 1024 * 1024 * 20, 3)),
			ConsoleLogger                (spdlog::stdout_color_mt("console"))
		{
			spdlog::set_async_mode(8192);
			spdlog::set_pattern("[%H:%M:%S] %v");
			spdlog::drop_all();
		}

		template<typename Arg1, typename... Args>
		inline void info(const char *fmt, const Arg1 &arg1, const Args &... args)
		{
			RotatedServerLogger->info(fmt, arg1, args...);
			ConsoleLogger->info(fmt, arg1, args...);
		}

		template<typename Arg1, typename... Args>
		inline void warn(const char *fmt, const Arg1 &arg1, const Args &... args)
		{
			RotatedServerLogger->warn(fmt, arg1, args...);
			ConsoleLogger->warn(fmt, arg1, args...);
		}

		template<typename Arg1, typename... Args>
		inline void error(const char *fmt, const Arg1 &arg1, const Args &... args)
		{
			RotatedServerLogger->error(fmt, arg1, args...);
			ConsoleLogger->error(fmt, arg1, args...);
		}

		template<typename T>
		inline void info(const T& msg)
		{
			RotatedServerLogger->info(msg);
			ConsoleLogger->info(msg);
		}

		template<typename T>
		inline void warn(const T& msg)
		{
			RotatedServerLogger->warn(msg);
			ConsoleLogger->warn(msg);
		}

		template<typename T>
		inline void error(const T& msg)
		{
			RotatedServerLogger->error(msg);
			ConsoleLogger->error(msg);
		}

	private:
		std::shared_ptr<spdlog::logger> RotatedServerLogger;
		std::shared_ptr<spdlog::logger> ConsoleLogger;
	};
}