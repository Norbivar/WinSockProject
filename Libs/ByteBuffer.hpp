#pragma once

#include <string>
#include <type_traits>
#include <iostream>

#include "GlobalIncludes.hpp"


#include <typeinfo>
namespace VN
{
    /*
    Pretty much an encapsulated character-array, which will then get sent through a socket.
    Each bytebuffer SHOULD start with exactly one OP-CODE and data following that OP-CODE according to the specification of the OP-CODES.

	v1.1
    */
    class ByteBuffer
    {
		#pragma region Variables
        private:
			// The current number of data inside the ByteArray (e.g.: the current number of '\0' charaters).
			uint8_t         m_CurrDataNumber;
			// The byte (char) array that the ByteBuffer accesses/possesses. Data is divided by '\0' characters.
			char* const     m_ByteArray;
			// Determines if the ByteBuffer is read-only (received) or write-only (sent).
			const bool      m_IsReadOnly;
			// Used to save the last position we read from (in the m_ByteArray).
			mutable size_t  m_CurrAtByte;

		#pragma endregion
		#pragma region Functions
        public:
			ByteBuffer() :  m_CurrDataNumber(0),
                            m_ByteArray(new char[cg_Buffer_Length + cg_Buffer_Offset]),
                            m_IsReadOnly(false),
                            m_CurrAtByte(0) 
			{ }

			ByteBuffer(char* target) :  m_CurrDataNumber(0),
			                            m_ByteArray(target),
			                            m_IsReadOnly(true),
			                            m_CurrAtByte(0)
			{ }

			ByteBuffer(const ByteBuffer&) = delete;
			ByteBuffer operator=(const ByteBuffer&) = delete;

			ByteBuffer(ByteBuffer&&) = delete;
			ByteBuffer operator=(ByteBuffer&&) = delete;

			~ByteBuffer() 
			{
				if(!m_IsReadOnly)
				{
					delete[] m_ByteArray;
				}
			}

			// Returns the start of the arary (pretty much the bytearray itself).
			const char* GetStart() const
			{
				return m_ByteArray;
			}

			// Returns the total occupied length of the m_ByteArray, which is exactly the position of the last ('\0' + 1).
			size_t GetLength() const;

		#pragma endregion
		#pragma region Reading/Writing
		private:
			template<typename T>
			void Put(const T& what)
			{
				if (!m_IsReadOnly)
				{
					// Character pointers
					if constexpr (std::is_same_v <std::decay<T>::type, char*> || std::is_same_v <std::decay<T>::type, const char*>)
					{
						int len = strlen(what);
						strcpy_s(&m_ByteArray[m_CurrAtByte], cg_Buffer_Length - m_CurrAtByte, what);
						m_CurrAtByte = m_CurrAtByte + len + 1;
					}
					// std::string
					else if constexpr (std::is_same_v <std::decay<T>::type, std::string>)
					{
						strcpy_s(&m_ByteArray[m_CurrAtByte], what.length(), what.c_str());
						m_CurrAtByte = m_CurrAtByte + obj.length() + 1;
					}
					// integers:
					else if constexpr (std::is_same_v <std::decay<T>::type, int> 
						            || std::is_same_v <std::decay<T>::type, long int>
						            || std::is_same_v <std::decay<T>::type, short int>)
					{
						snprintf(&m_ByteArray[m_CurrAtByte], cg_Buffer_Length - m_CurrAtByte - 1, "%d", what);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + 1;
					}
					// float:
					else if constexpr (std::is_same_v <std::decay<T>::type, float>)
					{
						snprintf(&m_ByteArray[m_CurrAtByte], cg_Buffer_Length - m_CurrAtByte - 1, "%f", what);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + 1;
					}
					// bool:
					else if constexpr (std::is_same_v <std::decay<T>::type, bool>)
					{
						snprintf(&m_ByteArray[m_CurrAtByte], cg_Buffer_Length - m_CurrAtByte - 1, "%d", what);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + 1;
					}
					// Client packets (strongly typed enums :/)
					else if constexpr (std::is_same_v <std::decay<T>::type, ServerPackets>
						            || std::is_same_v <std::decay<T>::type, ClientPackets>)
					{
						snprintf(&m_ByteArray[m_CurrAtByte], cg_Buffer_Length - m_CurrAtByte - 1, "%d", what);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + 1;
					}
					else
					{
						static_assert(false, "ByteBuffer Put(const T& what) was instantiated with an unhandled type!");
					}
					++m_CurrDataNumber;
				}
			}
		public:
			template<typename T>
			ByteBuffer& operator<<(const T& what)
			{
				Put<T>(what);
				return *this;
			}

		//=====================================================================
		
		private:
			template<typename T>
			T Next() const
			{
				if (m_IsReadOnly)
				{
					// Character pointers
					if constexpr (std::is_same_v <std::decay<T>::type, char*> || std::is_same_v <std::decay<T>::type, const char*>)
					{
						const char* pointer = &m_ByteArray[m_CurrAtByte];
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + cg_Buffer_Offset;
						return pointer;
					}
					// std::string
					else if constexpr (std::is_same_v <std::decay<T>::type, std::string>)
					{
						strcpy_s(&m_ByteArray[m_CurrAtByte], what.length(), what.c_str());
						m_CurrAtByte = m_CurrAtByte + obj.length() + 1;
					}
					// integers:
					else if constexpr (std::is_same_v <std::decay<T>::type, int> 
						|| std::is_same_v <std::decay<T>::type, long int> 
						|| std::is_same_v <std::decay<T>::type, unsigned int>
						|| std::is_same_v <std::decay<T>::type, unsigned long int>
						|| std::is_same_v <std::decay<T>::type, short>
						|| std::is_same_v <std::decay<T>::type, unsigned short>)
					{
						T i = atoi(&m_ByteArray[m_CurrAtByte]); // TODO: VALIDATE THIS, as 'atoi' returns an integer, which can vary in size
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + cg_Buffer_Offset;
						return i;
					}
					// float:
					else if constexpr (std::is_same_v <std::decay<T>::type, float>)
					{
						float f = atof(&m_ByteArray[m_CurrAtByte]);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + cg_Buffer_Offset;
						return f;
					}
					// bool:
					else if constexpr (std::is_same_v <std::decay<T>::type, bool>)
					{
						int i = atoi(&m_ByteArray[m_CurrAtByte]);
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + cg_Buffer_Offset;
						return (i != 0);
					}
					// enums -> packets:
					else if constexpr (std::is_same_v <std::decay<T>::type, ServerPackets>
						            || std::is_same_v <std::decay<T>::type, ClientPackets>)
					{
						T i = atoi(&m_ByteArray[m_CurrAtByte]); 
						m_CurrAtByte = m_CurrAtByte + strlen(&m_ByteArray[m_CurrAtByte]) + cg_Buffer_Offset;
						return i;
					}
					else
					{
						static_assert(false, "ByteBuffer Next() was instantiated with an unhandled type!");
					}
				}
				return NULL; // TODO: throw exception, as this MIGHT go unseen and cause bugs
			}

			public:
				template < typename T >
				const ByteBuffer& operator>>(T& into) const // this is not an universal ref because why the hell would we allow reading into a temporary
				{
					into = Next<T>();
					return *this;
				}
		#pragma endregion
    };
}